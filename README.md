

														=======================
														   HRM Interview BOT
														=======================

														
								/**
								 * @license Copyright (c) 2018, ERA-INFOTECH LIMITED. All rights reserved.
								 */											



# Dialogflow (Api.ai) - sample webhook implementation in Python Flask.

#This is a really simple webhook implementation that gets Dialogflow classification JSON (i.e. a JSON output of Dialogflow /query endpoint) 
and returns a fulfillment response.

# Using this powerful web service bot can respond to users collecting information from external web services and even Databases!!

More info about Dialogflow webhooks could be found here:
[Dialogflow Webhook](https://dialogflow.com/docs/fulfillment#webhook)

# Deploy to:
[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

# What does the service do?

Using this service chatBot could simulate a smiple interview with the candidates applied for a particular vacancies from our e-Rrecruitment.
Candidates can get some basic informations from this HRM-Bot as well. After successfully selected from the HR department, candiate can perform
this interview anytime with the bot and the data provided by the users will be saved in the database. So, HR department could check the answers
from different candidates later.

Regarding three tier architecture this application actually works as a loosely coupled middleware and maintains interactkon with Dialogflow using Webhook Protocol 
and communcate RESTFUL Web Services with python Request-Response mechanism. All these three tier applications have to work harmonically 
for completing a successfull response to a user.

Example:
BOT: From which university you have been graduated ? 
User: University X. 

#Features:

	1. Extended Database interaction capability.
	2. Fully supported with Python Request-Response mechanism.
	3. Could respond to multiple intents based on action parameters.
	4. Able to receive request parameters as JSON and prepare response for user intelligently. 
	5. Extract parametric values from user to generate sql query using different algoritms independently. 
	6. Nested requested could be invoked to interact external webservices as well as RESTFUL Web Services.
	7. User Authentication support added.
	8. SSL (Secured Socket Layer) included.
	
	
Regards, 

Developer : Salman Rakin
Project Manager: Anwar Hossain

Artificial Intelligence Team
ERA-INFOTECH LIMITED.
	

