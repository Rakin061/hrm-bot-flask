#!/usr/bin/env python



# @@  Author: Salman Rakin  @@
# @@  Supervisor: Anwar Hossain  @@
# @@  Artificial Intelligence Team @@
# @@  ERA-INFOTECH LIMITED @@



from __future__ import print_function
from future.standard_library import install_aliases
install_aliases()

from urllib.parse import urlparse, urlencode
from urllib.request import urlopen, Request
from urllib.error import HTTPError

import json
import os
import datetime
import re
import math

from flask import Flask
from flask import request
from flask import make_response

status_code="01"
flag=None
flag1 =0
date1="01/01/2017"
date2="07/20/2017"

# Flask app should start in global layout
app = Flask(__name__)


@app.route('/webhook', methods=['POST'])
def webhook():
    req = request.get_json(silent=True, force=True)

    print("Request:")
    print(json.dumps(req, indent=4))

    res = processRequest(req)

    res = json.dumps(res, indent=4)
    # print(res)
    r = make_response(res)
    r.headers['Content-Type'] = 'application/json'
    return r


def processRequest(req):
    if req.get("result").get("action") == "yahooWeatherForecast":

        baseurl = "https://query.yahooapis.com/v1/public/yql?"
        yql_query = makeYqlQuery(req)
        if yql_query is None:
            return {}
        yql_url = baseurl + urlencode({'q': yql_query}) + "&format=json"
        result = urlopen(yql_url).read()
        data = json.loads(result)
        res = makeWebhookResult(data)
        return res

    elif req.get("result").get("action") == "openweathermap":
        result = req.get("result")
        parameters = result.get("parameters")
        city = parameters.get("geo-city")

        print('City:-',city)

        if (city.upper()=='DHAKA'):
            city=city+',bd'

        API_KEY='e092bd9bb54133607de3a0f326a04144'

        baseurl='http://api.openweathermap.org/data/2.5/weather?q='+city+'&APPID='+API_KEY

        result = urlopen(baseurl).read()

        data = json.loads(result)

        if data['cod']==200:

            condition = data['weather'][0]['description']
            temperature = data['main']['temp']
            wind_speed = data['wind']['speed']

            speech = " Hello !! Today the weather in " + parameters.get("geo-city") + " is : " + condition + \
                     ", Temperature: " + str(int(temperature - 273)) + " C" + ",  Wind Speed: " + str(
                wind_speed) + " Kph.  Thanks!!"

        elif data['cod']==404:
            speech="Sorry! No city found named: "+parameters.get("geo-city")+". Please try with larger regions. Thanks!!"
        else:
            speech = "Sorry! Weather Information of the city: "+parameters.get("geo-city")+\
                     " is not available right now. Please try again later. Thanks !! "


        return {
           "speech": speech
        }



    elif req.get("result").get("action") == "Application.ID":

        result = req.get("result")
        parameters = result.get("parameters")
        application_id=parameters.get("App_ID")
        application_id=str(application_id)
        application_id=application_id.strip()

        baseurl="http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action="Application.ID"

        yql_url=baseurl+urlencode({'p': application_id})+ "&"+urlencode({'act': action})+ "&format=json"

        res = urlopen(yql_url).read()
        #test_res = urlopen(yql_url).read()
        data = json.loads(res)
        
        if data!= {}:
            flag = data['Status']['flag']
            name = data['Status']['result']


        if data=={}:
            speech="Sorry!! Your contact Number is not listed. May be you haven't applied for any post. Please Contact with our HR Department. Thanks!"
            return {
                "speech": speech,

                #"displayText": speech,
                # "data": data,
                "contextOut": [{"name":"in_contact_info", "lifespan":0,"parameters": {}},
                               {"name":"app_id", "lifespan":1,"parameters": {}},
                               {"name":"interview", "lifespan":0,"parameters": {}}],
                #"source": "apiai-weather-webhook-sample"
            }
        elif flag=='Y':
            speech = "No!! "+name+" ... You have already given the interview. No need to do it again. Thanks!."
            return {
                "speech": speech,
                "displayText": speech,
                # "data": data,
                "contextOut": [{"name":"in_contact_info", "lifespan":0,"parameters": {}},
                               {"name":"app_id", "lifespan":0,"parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
                "source": "apiai-weather-webhook-sample"
            }
        else:
            #name = data['Status']['result']
            speech = "So you're !     " + name + "....... I am initiating a short interview session for you. \\n\\n1. There will be <strong>40 MCQ</strong> type Questions.You have to choose the right answer !\\n2. Each Question will be displayed only once ! \\n3. You will get <strong>30 seconds</strong> to answer for each MCQ questions ! \\n4. Lastly there will a two written question for which You will get <strong>5 minutes</strong> each to answer! \\n\\nAre you ready?? "
            return {
                "speech": speech,
                "displayText": speech,
                # "data": data,
                "contextOut": [{"name":"interview", "lifespan":50,"parameters": {}}],
                "source": "apiai-weather-webhoo"
                          "k-sample"
            }





    elif req.get("result").get("action") == "Contact.No":

        result = req.get("result")
        parameters = result.get("parameters")
        contact_no=parameters.get("App_ID")
        contact_no=str(contact_no)
        contact_no=contact_no.strip()

        baseurl="http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action="Contact.No"

        yql_url=baseurl+urlencode({'p': contact_no})+ "&"+urlencode({'act': action})+ "&format=json"

        res = urlopen(yql_url).read()
        #test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data!= {}:
            flag = data['Status']['flag']
            name = data['Status']['result']

        if data=={}:
            speech="Sorry! Your contact Number is not listed. Please Contact with our HR Department. Thanks!"
            return {
                "speech": speech,
                "displayText": speech,
                # "data": data,
               "contextOut": [{"name":"in_contact_info", "lifespan":1,"parameters": {}},
                               {"name":"app_id", "lifespan":0,"parameters": {}},
                               {"name":"interview", "lifespan":0,"parameters": {}}],
                "source": "apiai-weather-webhook-sample"
            }
        elif flag=='Y':
            speech = "No! "+name+" ... You have already given the interview. No need to do it again. Thanks!."
            return {
                "speech": speech,
                "displayText": speech,
                # "data": data,
                "contextOut": [{"name":"in_contact_info", "lifespan":0,"parameters": {}},
                               {"name":"app_id", "lifespan":0,"parameters": {}},
                               {"name":"interview", "lifespan":0,"parameters": {}}],
                "source": "apiai-weather-webhook-sample"
            }
        else:
            name = data['Status']['result']
            speech = "So you're !     " + name + "....... I am initiating a short interview session for you. \\n\\n1. There will be <strong>40 MCQ </strong> type Questions.You have to choose the right answer !\\n2. Each Question will be displayed only once ! \\n3. You will get <strong>30 seconds</strong> to answer for each MCQ questions ! \\n4. Lastly there will two written question for which You will get <strong>5 minutes</strong> each to answer! \\n\\nAre you ready?? "
            return {
                "speech": speech,
                "displayText": speech,
                # "data": data,
                "contextOut": [{"name":"interview", "lifespan":50,"parameters": {}}],
                "source": "apiai-weather-webhook-sample"
            }







    elif req.get("result").get("action") == "Q.1":
        result = req.get("result")
        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named emp_id found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.1"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode({'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Sorry! I can't proceed your interview right now because my question bank is empty! Please contact with our HR! Thanks !! ",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if(options==' '):

                speech=question_desc+'\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q2", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q2", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }




    elif req.get("result").get("action") == "Q.2":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q1_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q2':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question2 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']


            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q3", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q3", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }



    elif req.get("result").get("action") == "Q.3":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q2_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q3':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']
            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q4", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q4", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }



    elif req.get("result").get("action") == "Q.4":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q3_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q4':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']
            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q5", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q5", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }


    elif req.get("result").get("action") == "Q.5":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q4_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q5':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']
            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q6", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q6", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }



    elif req.get("result").get("action") == "Q.6":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q5_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q6':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']
            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q7", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q7", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }


    elif req.get("result").get("action") == "Q.7":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q6_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q7':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']
            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q8", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q8", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.8":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q7_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q8':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q9", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q9", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.9":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q8_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q9':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q10", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q10", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.10":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q9_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q10':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q11", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q11", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.11":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q10_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q11':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q12", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q12", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }


    elif req.get("result").get("action") == "Q.12":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q11_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q12':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q13", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q13", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.13":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q12_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q13':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"


        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q14", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q14", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.14":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q13_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q14':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q15", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q15", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.15":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q14_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q15':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q16", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q16", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.16":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q15_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q16':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q17", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q17", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.17":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q16_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q17':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q18", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q18", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.18":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q17_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q18':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q19", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q19", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.19":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q18_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q19':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q20", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q20", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.20":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q19_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q20':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q21", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q21", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.21":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q20_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q21':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q22", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q22", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.22":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q21_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q22':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q23", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q23", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.23":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q22_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q23':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q24", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q24", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.24":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q23_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q24':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q25", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q25", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.25":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q24_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q25':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q26", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q26", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.26":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q25_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q26':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q27", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q27", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.27":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q26_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q27':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q28", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q28", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.28":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q27_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q28':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q29", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q29", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.29":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q28_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q29':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q30", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q30", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.30":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q29_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q30':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q31", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q31", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.31":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q30_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q31':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q32", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q32", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.32":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q31_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q32':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q33", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q33", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.33":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q32_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q33':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q34", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q34", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.34":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q33_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q34':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q35", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q35", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.35":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q34_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q35':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q36", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q36", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.36":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q35_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q36':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q37", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q37", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.37":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q36_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q37':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q38", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q38", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.38":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q37_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q38':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q39", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q39", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.39":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q38_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q39':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q40", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q40", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.40":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q39_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q40':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q41", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q41", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.41":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q40_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q41':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q42", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q42", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }
    elif req.get("result").get("action") == "Q.42":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q41_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q42':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q43", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q43", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.43":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q42_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q43':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q44", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q44", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.44":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q43_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q44':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q45", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q45", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.45":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q44_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q45':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q46", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q46", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.46":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q45_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q46':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q47", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q47", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.47":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q46_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q47':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q48", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q48", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.48":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q47_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q48':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q49", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q49", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.49":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q48_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q49':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q50", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q50", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.50":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q49_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q50':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q51", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q51", "lifespan": 1,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

    elif req.get("result").get("action") == "Q.51":
        result = req.get("result")
        parameters = result.get("parameters")
        answer = parameters.get('Q50_answer')

        cont = result.get("contexts")

        item_count = len(cont)
        index = -1

        for i in range(item_count):
            if cont[i]['name'] == 'interview':
                index = i

        if (index == -1):
            return {
                "speech": "No context named interview found. So, I can't proceed. Please contact developer."
            }
        else:
            contact_no = cont[index]['parameters']['App_ID']

        for i in range(item_count):
            if cont[i]['name'] == 'q51':
                index = i

        if (index == -1):
            return {
                "speech": "No context named Question3 found. So, I can't proceed. Please contact developer."
            }
        else:
            question_id = cont[index]['parameters']['Question1.ID']

        baseurl = "http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action = "Q.2"

        yql_url = baseurl + urlencode({'p': contact_no}) + "&" + urlencode(
            {'Question_ID': question_id}) + "&" + urlencode({'Answer': answer}) + "&" + urlencode(
            {'act': action}) + "&format=json"

        res = urlopen(yql_url).read()
        # test_res = urlopen(yql_url).read()
        data = json.loads(res)

        if data == {}:
            return {
                "speech": "Awesome! Your interview session ends here ! Hope to see You in our company!!",
                "contextOut": [{"name": "in_contact_info", "lifespan": 0, "parameters": {}},
                               {"name": "app_id", "lifespan": 0, "parameters": {}},
                               {"name": "interview", "lifespan": 0, "parameters": {}}],
            }
        else:
            question_id = data['Question_Details']['Question ID']
            question_desc = data['Question_Details']['Question Desc']
            options = data['Question_Details']['Options']

            if (options == ' '):

                speech = question_desc + '\\D'
                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q51", "lifespan": 0,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }

            else:

                opt = options.split(';')

                speech = question_desc + " \\n" + " \\r "

                for i in opt:
                    speech = speech + i + " , "

                speech = speech[:-3]

                return {
                    "speech": speech,
                    "contextOut": [
                        {"name": "q51", "lifespan": 0,
                         "parameters": {"Question1.ID": question_id}},
                    ]
                }






    elif req.get("result").get("action") == "Interview.Session":

        result = req.get("result")
        parameters = result.get("parameters")

        if parameters.get("contact_app")!="":
            contact_number=parameters.get("contact_app")
        if parameters.get("contact_info") != "":
            contact_number = parameters.get("contact_info")
        contact_number=str(contact_number)
        contact_number=contact_number.strip()
        #contact_app=contact_app.strip()

        ans_1=parameters.get("Q_1").strip()
        ans_2 = parameters.get("Q_2").strip()
        ans_3 = parameters.get("Q_3").strip()
        ans_4 = parameters.get("Q_4").strip()

        baseurl="http://202.40.190.116:8086/BotAPI-HRM/ApplicationStatus?"
        action="Interview.Session"

        yql_url=baseurl+ urlencode({'p': contact_number})+"&"+urlencode({'a1': ans_1})+"&"+urlencode({'a2': ans_2})+"&"+urlencode({'a3': ans_3})+"&"+urlencode({'a4': ans_4})+ "&"+urlencode({'act': action})+ "&format=json"

        res = urlopen(yql_url).read()

        speech="The interview ends here. We will contact with you soon. Thanks for your time and have a great day!!!!"



        return {
            "speech": speech,
            "displayText": speech,
            # "data": data,
            "contextOut": [{"name":"in_contact_info", "lifespan":0,"parameters": {}},
                            {"name":"app_id", "lifespan":0,"parameters": {}},
                            {"name":"interview", "lifespan":0,"parameters": {}}],
            "source": "apiai-weather-webhook-sample"
        }



    else:
        speech="Problem in Action Matching. Please Contact Developer."
        return {
            "speech": speech,
            "displayText": speech,
            # "data": data,
            # "contextOut": [],
            "source": "apiai-weather-webhook-sample"
        }




def makeYqlQuery(req):
    result = req.get("result")
    parameters = result.get("parameters")
    city = parameters.get("geo-city")
    if city is None:
        return None

    return "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='" + city + "')"


def makeWebhookResult(data):
    query = data.get('query')
    if query is None:
        return {}

    result = query.get('results')
    if result is None:
        return {}

    channel = result.get('channel')
    if channel is None:
        return {}

    item = channel.get('item')
    location = channel.get('location')
    units = channel.get('units')
    if (location is None) or (item is None) or (units is None):
        return {}

    condition = item.get('condition')
    if condition is None:
        return {}

    # print(json.dumps(item, indent=4))

    temp= condition.get('temp')
    temp=int(temp)
    temp = ((temp-32)*5)/9
    temp=math.floor(temp)
    temp=str(temp)


    speech = " Hello !! Today the weather in " + location.get('city') + " is : " + condition.get('text') + \
             ", and the temperature is " + temp + " " + "C" + ".  Thanks!!"

    print("Response:")
    print(speech)

    return {
        "speech": speech,
        "displayText": speech,
        # "data": data,
        # "contextOut": [],
        "source": "apiai-weather-webhook-sample"
    }

if __name__ == '__main__':
    port = int(os.getenv('PORT', 5001))

    print("Starting app on port %d" % port)

    app.run(debug=False, port=port, host='0.0.0.0')
