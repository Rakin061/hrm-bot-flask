create procedure DPR_BOT_QUES_SET(pRCCMPCDE in varchar2, 
                                  pUSER_ID in varchar2,
                                  pMsgFlg out varchar2,
                                  pMsgDes out varchar2)
is
begin
    ORBERA.DPR_QUES_SET_APL(pRCCMPCDE=>pRCCMPCDE,
                                  pUSER_ID =>pUSER_ID,
                                  pMsgFlg =>pMsgFlg,
                                  pMsgDes =>pMsgDes);
end;
/

